from pathlib import Path
import torch
from torchvision import models
from Image.Image import Image
import torch.nn as nn

from Image.ImageCreator import ImageCreator
from config import CRITERION

def set_parameter_requires_grad(model, feature_extracting):
    if feature_extracting:
        print('Disable Gradiente Before new Layer')
        for param in model.parameters():
            param.requires_grad = False

def initialize_model(model_name, num_classes, feature_extract, use_pretrained=True):
    model_ft = models.resnet18(pretrained=use_pretrained)
    set_parameter_requires_grad(model_ft, feature_extract)
    num_ftrs = model_ft.fc.in_features
    model_ft.fc = nn.Linear(num_ftrs, num_classes)
    input_size = 250
    return model_ft, input_size

def train_model(model, train_loader, optimizer, device):
    model.to(device)
    model.train()
    train_loss = 0.0

    for data, labels in train_loader:
        data, labels = data.to(device), labels.to(device).long().squeeze()
        optimizer.zero_grad()
        target = model(data.permute(0, 3, 2, 1))
        loss = CRITERION(target,labels)
        loss.backward()
        optimizer.step()
        train_loss += loss.item()
    loss_ep = loss/len(train_loader)
    return loss_ep

def val_model(model, val_loader,device):
    valid_loss = 0.0
    model.eval()
    total = 0
    correct = 0

    with torch.no_grad():
        for data, labels in val_loader:
            data, labels = data.to(device), labels.to(device).long().squeeze()
            target = model(data.permute(0, 3, 2, 1))
            loss = CRITERION(target,labels)
            valid_loss += loss.item()
            predicted = torch.max(target, axis=1)[1]
            total += len(labels)
            correct += (predicted == labels).type(torch.float).sum().item()

    acc_ep = correct/total
    loss_ep = loss/len(val_loader)
    return acc_ep,loss_ep

def load_image_array(directory_path: Path, image_creator: ImageCreator) -> list[Image]:
    image_paths: list[Path] = [file for file in directory_path.iterdir() if not file.is_dir()]
    return [image_creator.create(image_path) for image_path in image_paths]