from torch import FloatTensor, IntTensor
from torch.utils.data import Dataset

from Image.Image import Image


class ImageDataset(Dataset):
    _image_array: list[Image]
    
    def __init__(self, image_array: list[Image]):
        self._image_array = image_array
        
        super().__init__()

    def __len__(self):
        return len(self._image_array)

    def __getitem__(self, idx: int) -> tuple[FloatTensor, IntTensor]:
        image: Image = self._image_array[idx]
        return FloatTensor(image.source), IntTensor([int(image.getType())])