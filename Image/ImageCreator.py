
from enum import IntEnum
from pathlib import Path
from Image.Image import Image


class ImageCreator:
    def __init__(self, image_type: IntEnum):
        self._image_type = image_type
        
    def create(self, image_path: Path):
        return Image(image_path, self._image_type)
