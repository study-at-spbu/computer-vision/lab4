from enum import IntEnum


class MonkeyImageType(IntEnum):
    BALD_UAKARI = 0
    EMPEROR_TAMARIN = 1
    GOLDEN_MONKEY = 2
    GRAY_LANGUR = 3
    HAMADRYAS_BABOON = 4
    MANDRIL = 5
    PROBOSCI_MONKEY = 6
    RED_HOWLER = 7
    VERVET_MONKEY = 8
    WHITE_FACED_SAKI = 9
    
    def to_str(self):
        str_array: dict[int, str] = {
            0: 'Bald Uakari',
            1: 'Emperor Tamarin',
            2: 'Golden Monkey',
            3: 'Gray Langur',
            4: 'Hamadryas Baboon',
            5: 'Mandril',
            6: 'Proboscis Monkey',
            7: 'Red Howler',
            8: 'Vervet Monkey',
            9: 'White Faced Saki'
        }
        
        return str_array[self.value]