from enum import IntEnum


class CarBikeType(IntEnum):
    CAR = 0
    BIKE = 1
    
    def to_str(self):
        str_array: dict[int, str] = {
            0: 'Car',
            1: 'Bike',
        }
        
        return str_array[self.value]