from enum import IntEnum
from pathlib import Path
from cv2.typing import MatLike
import cv2

from config import IMGSIZE

class Image:
    source: MatLike
    
    def __init__(self, image_path: Path, image_type: IntEnum):
        image_data: MatLike = cv2.imread(filename=str(image_path.resolve()))
        image_data = cv2.resize(image_data,IMGSIZE,interpolation=cv2.INTER_AREA)
        image_data = cv2.cvtColor(image_data, cv2.COLOR_BGR2RGB)
        self.source = image_data
        self._image_type = image_type
    
    def getType(self) -> IntEnum:
        return self._image_type